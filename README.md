## Obteniendo entradas de usuario y listas

Para este ejercicio es importante documentarse acerca del uso de algunos métodos y funciones nativas de Python para trabajar con listas.

Define la función `user_list` que tomará un número `num` del usuario como primera entrada. Este número le indicará al programa el número de entradas y comandos que el usuario escribirá en la terminal. 

Usa los tres ejemplos de entradas del usuario y pruebas que se presentan en el siguiente `driver code` para verificar si tu programa pasa las pruebas correspondientes:

```python
#driver code
"""Ejemplo 1 de entradas de usuario"""

5
insert 0 7
insert 1 8
insert 2 9
insert 3 3
insert 0 15

print(user_list() == [15, 7, 8, 9, 3])
# True

"""Ejemplo 2 de entradas de usuario"""

11
insert 0 7
insert 1 8
insert 2 9
insert 3 3
insert 0 15
remove 9
append 90
append 100
remove 8
append 20
sort

print(user_list() == [3, 7, 15, 20, 90, 100])
# True

"""Ejemplo 3 de entradas de usuario"""

17
insert 0 7
insert 1 8
insert 2 9
insert 3 3
insert 0 15
remove 9
append 9
append 90
append 100
remove 8
append 20
sort
pop
reverse
pop
reverse
insert 0 47

print(user_list() == [47, 7, 9, 15, 20, 90])
# True
```